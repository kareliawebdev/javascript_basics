/*
Tee puhelinluettelo.
Puhelinluetteloon lisäät taulukkoon objekteja (eli henkilöitä joilla nimi ja
puhelinnumero). Käyttäjältä kysytään henkilön nimi ja puhelinnumero.
Henkilön poisto -toimintoa ei tarvitse tässä versiossa olla.

Tee hakutoiminto jossa haet nimen perusteella puhelinnumeron.

Tee puhelinnumeron haku funktioksi.
Funktion parametrina on taulukko josta haetaan ja henkilön nimi.
Funktio palauttaa puhelinnumeron.
Kutsu funktiota.

Käyttöliittymän voit tehdä millaiseksi haluat (komentokehoite -pohjainen kuitenkin)

**************************************************************

Make a phonebook. Into the phonebook objects (persons that have name and 
phone number) are added. There is no need to make remove person function into
this version.

Make a search function that uses name to receive the phone number attached to it.

The search function should have an array as a parameter from where there is
a search conducted and name searched from. Function should return a phone number.

Call the function.

User interface you can make into whatever you want (CLI though).

**************************************************************

let henkilot = [{nimi:"Aku Ankka", ika:55},{nimi:"Hessu Hopo", ika:45}];

let uusihenkilo={nimi:"Iines Ankka", ika:55};

henkilot.push(uusihenkilo);

*/
function searchPhonebook(phonebookToSearch, nameToSearch) {
  function isInArray(contact) {
    return contact.name === nameToSearch;
  }
  let foundNumber = phonebookToSearch.find(isInArray);
  return foundNumber;
}

function addPerson() {
  let nameInput = input.question("Anna henkilon nimi: ");
  let ageNotOk = true;
  let ageInput;
  while (ageNotOk) {
    ageInput = input.question("Anna ko. henkilon ika: ");
    parseInt(ageInput, 10);
    if (ageInput >= 0 && !isNaN(ageInput)) {
      ageNotOk = false;
    } else {
      console.log("ika tulisi olla numero ja positiivinen arvo");
    }
  }
  let phoneNumberInput = input.question("Anna ko. henkilon puhelinnumero: ");
  let person = new Object();
  person.name = nameInput;
  person.age = ageInput;
  person.phonenumber = phoneNumberInput;
  phonebook.push(person);
  console.log(
    "\n\n\thenkilo " +
      phonebook[phonebook.length - 1].name +
      " lisattiin puhelinluetteloon.\n\n"
  );
}

console.log(
  "**************************\n" +
    "* Puheliluettelosovellus *\n" +
    "**************************"
);
var input = require("readline-sync");
var looping = true;
const phonebook = [];

// while loop to keep program running
while (looping) {
  let selection = input.question(
    "valitse toiminto\n1) hae puhelinnumeroa\n" +
      "2) lisaa yhteystieto.\n" +
      "3) sulje puhelinluettelo \nValinta: "
  );

  // logic for main selection
  switch (selection) {
    case "1":
      let nameToSearch = input.question("kenen numeron haluat loytaa: ");
      let result = searchPhonebook(phonebook, nameToSearch);
      if (typeof result !== "undefined") {
        console.log(
          "\n\n\t" + nameToSearch + " numero on: " + result.phonenumber + "\n\n"
        );
      } else {
        console.log("\n\n\tyhteystietoja ei loytynyt.\n\n");
      }
      break;
    case "2":
      addPerson();
      break;
    case "3":
      looping = false;
      break;

    default:
      console.log("\n\n\tValittua toimintoa ei loydy!\n\n");
  }
}
