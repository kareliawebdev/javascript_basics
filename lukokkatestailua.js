const readline = require("readline-sync");

// tehtävä 1
/*Luo skripti jossa alustat taulukon (nimeltään kuukaudet) vuoden kuukausilla.
Tulosta kuukaudet toisinpäin for silmukassa ts. joulukuusta tammikuuhun. Käytä laskurimuuttujana kk:ta.
PS. Käytä suomenkielistä merkintää (ts. esim. muuttujat on määriteltävä juuri yo. nimisiksi)*/

/*const kuukaudet = [
  "tammikuu",
  "helmikuu",
  "maaliskuu",
  "huhtikuu",
  "toukokuu",
  "kesakuu",
  "heinakuu",
  "elokuu",
  "syyskuu",
  "lokakuu",
  "marraskuu",
  "joulukuu",
];
console.log(kuukaudet);
const kk = kuukaudet.reverse();
console.log(kk);

kk.forEach((element) => {
  console.log(element);
});

for (let i = 0; i < kk.length; i++) {
  console.log(kk[i]);
}*/

//tehtava 2
/*const kanta = readline.question("anna kolmion kanta: ");
const korkeus = readline.question("anna kolmion korkeus: ");
const pinta_ala = (kanta * korkeus) / 2;
console.log(pinta_ala);*/

//tehtava 3
/*function HenkTieto(ika) {
  if (ika < 18 && ika >= 0) {
    return "nuori";
  } else if (ika >= 18 && ika <= 65) {
    return "aikuinen";
  } else if (ika > 65) {
    return "vanhus";
  }
}
console.log(HenkTieto(0.0));*/

/*tehtävä 4
Luo luokka Elakelainen, jonka ominaisuuksia ovat etunimi, sukunimi, ika,onko_ajokortti
Etunimi ja sukunimi ovat merkkijonoja.
Ikä(ika) on numero.
onko_ajokortti on boolean tyyppinen muuttuja.
Sijoita attribuuttien arvot muodostimessa.
Voit kokeilla toimiiko luokasta tehty olio, mutta palauta tähän tehtävään vain
luokka Elakelainen (joka on testattu).*/
/*class Elakelainen {
  constructor(etunimi, sukunimi, ika, onko_ajokortti) {
    try {
      this.etunimi = etunimi;
      this.sukunimi = sukunimi;
      this.ika = ika;

      if (typeof onko_ajokortti == "boolean") {
        this.onko_ajokortti = onko_ajokortti;
      } else {
        throw "not boolean";
      }
    } catch (err) {
      console.log("onko_ajokortti variable should be boolean, now is " + err);
    }
  }
}
const elakeIhminen = new Elakelainen("matti", "komulainen", 67, false);
console.log(
  elakeIhminen.etunimi +
    elakeIhminen.sukunimi +
    elakeIhminen.ika +
    elakeIhminen.onko_ajokortti
);*/

//tehtävä 5
/*Luo funktio Kaanto, jossa on parametri str
Kaanto -funktio kääntää merkkijono ja palauttaa
käännetyn merkkijonon.*/
/*function Kaanto(str) {
  let kaannetty = "";
  for (let i = str.length - 1; i > -1; i--) {
    kaannetty += str[i];
  }
  return kaannetty;
}

console.log(Kaanto("jotain"));*/
