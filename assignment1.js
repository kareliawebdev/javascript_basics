/*
Tee sovellus, joka kysyy syötettävän sanan (merkkijono).
Sovellus testaa onko sana palindromi ts. sana on sama toisinpäin.
Esim. sana saippuakauppias on palindromi.
Luo ratkaisu siten että et käytä valmiita funktioita vaan teet oman algoritmin.

Muuta edellinen ratkaisu funktioksi joka palauttaa boolean arvon true, mikäli funktion
parametrina oleva sana on palindromi (jos ei, niin palautetaan false).

Kutsu funktiota.

********************************************************************************

Make an application that asks a word as input (string). The application should
test whether given word is a palindrome or not. For example word saippuakauppias
is a palindrome. Don't use ready fuctions, make your own algorithm.

Change your solution to a function that returns a boolean value of true if a word
passed as parameter is a palindrome (of course if it is not, it should return false).

Call the function


*/

function checkIfPalindrome(word) {
  let word_palindrome = false;

  // Math.floor used to// round down needed looping times.
  // Character in the middle needs not
  // to be checked (same character reading backwards).
  for (let i = 0; i < Math.floor(word.length / 2); i++) {
    if (word[i] == word[word.length - 1 - i]) {
      word_palindrome = true;
    } else {
      return false;
    }
  }
  return word_palindrome;
}

var input = require("readline-sync");
let word_input = input.question("please type in a word and press enter: ");
word_palindrome = checkIfPalindrome(word_input);
if (word_palindrome) {
  console.log("word is palindrome");
} else {
  console.log("word is not palindrome");
}
