/* Tehtävä 4a
Luo REST API sanakirjaa varten.

Sanakirjassa (tieto tiedostossa - esim. sanakirja.txt) on taulukossa eri riveillä 
sana suomeksi ja englanniksi; esim.

auto car
talo house

Tarvitset API:a varten luoda hakua varten metodin (toteuta GET metodi) sekä sanojen 
lisäystä varten metodin (toteuta POST metodi).

Tiedonhaussa parametrina on suomenkielinen sana. Haku palauttaa englannin kielisen 
sanan vastineen.

Tiedonlisäyksessä tallennetaan suomenkielinen sana ja vastaava englanninkielinen 
sana uudelle riville (tekstitiedostoa kasvatetaan uudella rivillä)

Tiedoston käsittelyä varten tarvitset fs moduulin ( require("fs")). 

Em. moduulista löytyy esim. metodi writeFileSync kirjoitusta varten ja 
readFileSync lukemista varten.

(Tiedoston käsittely voi olla synkroninen tai asynkroninen  - saa valita).
*/

/* Constant variables */
const express = require("express");
const HandleFile = require("./handleFile");
const pathToDictionary = "./dictionary/dictionary.txt";
const pahtToHTMLform = "./tallennuslomake.html";
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// CORS settings (Cross-Origin Resource Sharing)
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  res.setHeader("Content-type", "application/json");
  next();
});

// *** create an object to handle file ***
const fh = new HandleFile(pathToDictionary);
const formHTML = new HandleFile(pahtToHTMLform);

// return whole dictionary array
app.get("/dictionary", (req, res) => {
  let dataFromFile = fh.loadData();
  res.json(dataFromFile);
});

app.get("/", (req, res) => {
  let formFromFile = formHTML.loadHTML();
  res.setHeader("Content-Type", "text/html;charset=utf-8");
  res.send(formFromFile);
});

// *** get word from dictionary ***
app.get("/find/:word", (req, res, next) => {
  let dataFromFile = fh.loadData();
  console.log(req.params.word);

  for (let i = 0; i < dataFromFile.length; i++) {
    let result = [];
    dataFromFile.forEach((element) => {
      if (Object.keys(element) == req.params.word) {
        result.push(element);
      }
    });

    res.json(result);
    next();
  }
});

// put word into dicitonary

app.post("/add-word", (req, res, next) => {
  let wordPair = req.body;

  // check if key is already in dictionary
  function checkIfWordInDictionary(wordPair) {
    let dataFromFile = fh.loadData();
    let hit = false;

    for (const element of dataFromFile) {
      if (
        // to uppercase to lose case sensitivity
        Object.keys(element)[0].toUpperCase() ==
          Object.keys(wordPair)[0].toUpperCase() &&
        Object.values(element)[0].toUpperCase() ==
          Object.values(wordPair)[0].toUpperCase()
      ) {
        // if word is in dicitonary, check if eng word is too
        // if word pair is found return
        console.log("osuma");
        hit = true;
        res
          .status(200)
          .json(
            "word pair " +
              Object.keys(wordPair)[0] +
              "-" +
              Object.values(wordPair)[0] +
              " allready exists in dictionary. no action taken"
          );

        break;
      }
    }

    if (!hit) {
      fh.writeData(wordPair);
      res
        .status(201)
        .json(
          "word pair " +
            Object.keys(wordPair)[0] +
            "-" +
            Object.values(wordPair)[0] +
            " added to dictionary file"
        );
    }
  }

  checkIfWordInDictionary(wordPair);

  next();
});

app.listen(port, () => {
  console.log(`server listening port ${port}`);
});

/* Tehtävä 4b

Luo edellä tehtyyn (tehtävässä 4a) REST APIiin käyttöliittymä, jonka avulla voit lisätä ja hakea sanoja.

Käytä käyttöliittymässä jQueryä ja html:ää.

Lisää REST APIin ennen metodien käsittelyä:

//CORS isn’t enabled on the server, this is due to security reasons by default,
//so no one else but the webserver itself can make requests to the server.
// Add headers
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  res.setHeader("Content-type", "application/json");

  // Pass to next layer of middleware
  next();
});
Käyttöliittymän runko tallennuslomakkeeseen:
<!DOCTYPE html>
<html>
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
      $(document).ready(function () {
        $('[name="Submit"]').click(function () {
       
          //tähän omaa koodia
          });
       });
    </script>
  </head>
  <body>
   
 <form id="form" name="form">
      <th>Sanapari</th>
      <td>
        <input
          name="fi"
          id="fi"
          type="text"
          value=""
          maxlength="35"
          size="35"
        />
      </td>
      <td>
        <input
          name="en"
          id="en"
          type="text"
          value=""
          maxlength="35"
          size="35"
        />
      </td>

      <td>
        <input name="Submit" type="button" value="Tallenna" />
      </td>
    </form>
  </body>
</html>
Voit luoda oman sivun hakua varten jossa on esim. yhdistelmäruutu.
Tee valikko, josta pääset liikkumaan lisäyssivulle ja hakusivulle.
Tässä harjoituksessa on tyyli vapaa ts. saat muotoilla tyylin.
VAIHTOEHTO 2 TÄLLE TEHTÄVÄLLE:
Luo komentokehoite -pohjainen käyttöliittymä, jossa haet axios -kirjaston avulla edellä tehdystä backendistä (REST APIsta tehtävässa 4a) käännetyn sanan hakuehdon perusteella (hakuehto siis suomalainen sana ja REST API palauttaa käännetyn sanan). Voit katsoa mallia tästä (kts. kohta:"How to POST request using the Axios in JavaScript?"), miten voit kutsua axios -kirjaston avulla REST APIa. */
