/*HandleFile class object handles file operations for REST1 assignment*/
class HandleFile {
  #fs = require("fs");
  #filename;
  constructor(filename) {
    this.#filename = filename;
  }

  set file(filename) {
    this.#filename = filename;
  }
  get file() {
    return this.#filename;
  }

  // ** Methods **

  // load html-file
  loadHTML() {
    let htmlFromFile = this.#fs.readFileSync(
      this.#filename,
      { encoding: "utf8", flag: "r" },
      function (err) {
        if (err) throw err;
      }
    );
    let htmlString = htmlFromFile.toString();
    return htmlString;
  }

  /****   loadData method reads data from file and splits text to elements
  into an array.
  
  Returns an object with finnish words as keys and english words as values for those keys  *****/
  loadData() {
    let textFromFile = this.#fs.readFileSync(
      this.#filename,
      { encoding: "utf8", flag: "r" },
      function (err) {
        if (err) throw err;
      }
    );
    let textFromFileArray = textFromFile.toString().split("\n");

    // If the last element is an empty string, pop it away (we need this for nothing).
    if (textFromFileArray[textFromFileArray.length - 1] == "") {
      textFromFileArray.pop();
    }

    // Lets make an array of objects to make it little easier to handle data
    let dictionary = [];
    for (let i = 0; i < textFromFileArray.length; i++) {
      let suomiEng = textFromFileArray[i].split(" ");
      if (suomiEng.length != 2) {
        throw "no word pair in line " + i + 1;
      }
      let suo = suomiEng[0];
      let eng = suomiEng[1];
      let dictObject = {};
      dictObject[suo] = eng;

      dictionary.push(dictObject);
    }

    return dictionary;
  }

  /******* writeData method appends given object key and value to file.   *******/
  writeData(dataToFile) {
    console.log(
      Object.keys(dataToFile)[0],
      Object.values(dataToFile)[0],
      "written to file"
    );
    this.#fs.appendFileSync(
      this.#filename,
      Object.keys(dataToFile)[0] + " " + Object.values(dataToFile)[0] + "\n",
      "utf8"
    );
  }
}

// ******************    testing    ******************************

//const kokeilu = new HandleFile("./dictionary/test.txt");

//write some data to file
// let taulu = { hyrysysy: "car" };
// kokeilu.writeData(taulu);

//load data from file
// let tauluTiedostosta = kokeilu.loadData();
// console.log(tauluTiedostosta);

// file not to be found
/*
kokeilu.file = "jotainmuuta.txt";
console.log(kokeilu.file);
let tauluTiedostosta2 = kokeilu.loadData();
console.log(tauluTiedostosta2);*/

module.exports = HandleFile;
