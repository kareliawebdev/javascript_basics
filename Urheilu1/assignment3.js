/*Tehtävä: Urheilu 1

Kehitystehtävänä on määritellä olio-ohjelmointikielille ominainen luokkamäärittely ja periytyminen
JavaScript-kielellä.

Määrittele yliluokka Henkilo, joka sisältää ihmisen henkilötietoja:

etunimet,
sukunimi,
kutsumanimi,
syntymävuosi
Määrittele luokka Urheilija, joka perii Henkilo-luokan ja toteuttaa lisäksi saantifunktiot
(get- ja set-) Urheilija-luokalle merkityksellisiin attribuutteihin. Lisää Urheilija luokkaan
seuraavat ominaisuudet:

- linkki kuvaan,
- omapaino,
- laji,
- saavutukset.

Kirjoita nämä vaatimukset toteuttava koodi joka toimii node.js-tulkissa.

1. Toteuta koodi. Lisää koodiin esimerkkejä Urheilija –olioista.
2. Talleta toteutuksesi gitlab (tai github) -ympäristöön omaan projektiisi hakemistoon: Urheilu1
3. Kirjoita tehtävästä kommentointi ja huomioita kehittäjän blogiin (hackmd.io).

Voitte keskustella tehtävän suoritukseen liittyvistä asioista Slackissa - älkää kuitenkaan antako
valmiita vastauksia, vaan opastakaa ja ohjatkaa askeleittain eteenpäin.*/

class Henkilo {
  #etunimet;
  #sukunimi;
  #kutsumanimi;
  #syntymavuosi;

  constructor(etunimet, sukunimi, kutsumanimi, syntymavuosi) {
    this.#etunimet = etunimet;
    this.#sukunimi = sukunimi;
    this.#kutsumanimi = kutsumanimi;
    this.#syntymavuosi = syntymavuosi;
  }

  get _etunimet() {
    return this.#etunimet;
  }

  set _etunimet(etunimet) {
    this.#etunimet = etunimet;
  }
  get _sukunimi() {
    return this.#sukunimi;
  }

  set _sukunimi(sukunimi) {
    this.#sukunimi = sukunimi;
  }
  get _kutsumanimi() {
    return this.#kutsumanimi;
  }

  set _kutsumanimi(kutsumanimi) {
    this.#kutsumanimi = kutsumanimi;
  }
  get _syntymavuosi() {
    return this.#syntymavuosi;
  }

  set _syntymavuosi(syntymavuosi) {
    this.#syntymavuosi = syntymavuosi;
  }
}

class Urheilija extends Henkilo {
  #linkkiKuvaan;
  #omapaino;
  #laji;
  #saavutukset;

  constructor(
    etunimet,
    sukunimi,
    kutsumanimi,
    syntymavuosi,
    linkkiKuvaan,
    omapaino,
    laji,
    saavutukset
  ) {
    super(etunimet, sukunimi, kutsumanimi, syntymavuosi);
    this.#linkkiKuvaan = linkkiKuvaan;
    this.#omapaino = omapaino;
    this.#laji = laji;
    this.#saavutukset = saavutukset;
  }

  get _linkkiKuvaan() {
    return this.#linkkiKuvaan;
  }
  set _linkkiKuvaan(linkkiKuvaan) {
    this.#linkkiKuvaan = linkkiKuvaan;
  }

  get _omapaino() {
    return this.#omapaino;
  }
  set _omapaino(omapaino) {
    this.#omapaino = omapaino;
  }

  get _laji() {
    return this.#laji;
  }
  set _laji(laji) {
    this.#laji = laji;
  }

  get _saavutukset() {
    return this.#saavutukset;
  }
  set _saavutukset(saavutukset) {
    this.#saavutukset = saavutukset;
  }
}

const urheilija = new Urheilija(
  ["matti", "teppo", "kalle"],
  "jaakkola",
  "tepi",
  1992,
  "link",
  84,
  "kirveen nosto kaivosta",
  [{ nagano: "kulta" }, { helsinki: "hopea" }, { tokio: "pronssi" }]
);

const urheilija2 = new Urheilija(
  ["minna", "jonna", "tiina", "matilda"],
  "perttula",
  "minz",
  1988,
  "link2",
  67,
  "kirveen heitto kaivoon",
  [{ nagano: "bronssi" }, { helsinki: "4." }, { tokio: "diskattu" }]
);

console.log(urheilija._etunimet);
console.log(urheilija._kutsumanimi);
console.log(urheilija._omapaino);
console.log(urheilija._saavutukset);

console.log(urheilija2._etunimet);
console.log(urheilija2._kutsumanimi);
console.log(urheilija2._omapaino);
console.log(urheilija2._saavutukset);

urheilija._kutsumanimi = "tepikka";
urheilija._omapaino = 82;
console.log(urheilija._kutsumanimi);
console.log(urheilija._omapaino);

urheilija2._kutsumanimi = "minzu";
urheilija2._omapaino = 70;
console.log(urheilija2._kutsumanimi);
console.log(urheilija2._omapaino);

Urheilija.prototype.reppu = "flällraven";

console.log(urheilija2.reppu);
console.log(urheilija.reppu);
const henk = new Henkilo();
console.log(henk.reppu);
